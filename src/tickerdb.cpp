#include "tickerdb.h"

#include "factory.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>

#include <QVariant>

TickerDB::TickerDB(size_t index, QObject *parent) :
    Trader("TDb", index, parent),
    db(NULL)
{
}

void TickerDB::addOptions(std::vector<Trader*> *v, OptionParser *op)
{
    op->add(new Option::Multiple<TickerDB*, false, Trader*>(
                v, "tickerdb", 't', "Adds a database for Ticker data"));
}

const int TickerDB::classId = Factory<Trader>::registerClass(&(TickerDB::addOptions),
                                                        "tickerdb");

void TickerDB::analyzeTicker(Ticker ticker)
{
    QSqlQuery query;
    query.prepare("INSERT INTO " + m_name + " ("
                  "time,"
                  "price,"
                  "buy,"
                  "sell,"
                  "high,"
                  "low,"
                  "volume,"
                  "month_avg,"
                  "month_vol,"
                  "local_cur,"
                  "remote_cur"
                  ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    query.addBindValue(ticker.date.toMSecsSinceEpoch());
    query.addBindValue(ticker.price.price);
    query.addBindValue(ticker.price.buy);
    query.addBindValue(ticker.price.sell);
    query.addBindValue(ticker.high);
    query.addBindValue(ticker.low);
    query.addBindValue(ticker.volume);
    query.addBindValue(ticker.monthAvg);
    query.addBindValue(ticker.monthVolume);
    query.addBindValue(ticker.local);
    query.addBindValue(ticker.remote);
    if (!query.exec()) {
        qCritical() << "Query error:" << db->lastError().text();
    }
}

bool TickerDB::connectObjects(std::vector<Broker*> *brokers, std::vector<Trader*> *, QSqlDatabase *db)
{
    this->db = db;

    for (unsigned int i = 0; i < brokers->size(); i++)
    {
        if (brokers->at(i)->claimRead(getClassId()))
        {
            QObject::connect(brokers->at(i), SIGNAL(tick(Ticker)),
                             this, SLOT(analyzeTicker(Ticker)));
            rename(brokers->at(i)->name());
            return openDb();
        }

    }
    qCritical("[%s] No accessible Broker found.", qPrintable(m_name));
    return false;
}

bool TickerDB::openDb()
{
    if (!db->tables().contains(m_name)) {
        QSqlQuery q = db->exec("CREATE TABLE " + m_name + "("
                              "time INTEGER PRIMARY KEY,"
                              "price REAL,"
                              "buy REAL,"
                              "sell REAL,"
                              "high REAL,"
                              "low REAL,"
                              "volume REAL,"
                              "month_avg REAL,"
                              "month_vol REAL,"
                               "local_cur INTEGER,"
                               "remote_cur INTEGER"
                              ");");
        if (q.lastError().isValid())
        {
            qCritical() << "Could not create table:" << db->lastError().text();
            return false;
        }
    }
//    else
//    {
//        QSqlQuery q = db.exec("DELETE FROM ticker;");
//        if (q.lastError().isValid())
//        {
//            qCritical() << "Could not empty table:" << db.lastError().text();
//            return false;
//        }
//    }
    return true;
}
