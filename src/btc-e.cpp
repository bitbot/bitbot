#include "btc-e.h"

#include <QtNetwork/QNetworkReply>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>
#include <QMessageAuthenticationCode>

#include <QStringList>

#include "factory.h"

namespace api {
static const char *baseUrl = "https://btc-e.com";
static const char *basePath = "/tapi";

}

const char* BtcE::getMethod(Operation o)
{
    switch (o) {
    case opTrade:
        return "Trade";
    case opGetInfo:
        return "getInfo";
    case opTransHistory:
        return "TransHistory";
    case opTradeHistory:
        return "TradeHistory";
    case opActiveOrders:
        return "ActiveOrders";
    case opCancelOrder:
        return "CancelOrder";
    default:
        return "";
    }
}

quint64 getNonce()
{
    static quint64 lastnonce = 0;
    quint64 nonce = QDateTime::currentMSecsSinceEpoch() / 1000;

    if (nonce <= lastnonce)
        nonce = lastnonce + 1;
    lastnonce = nonce;

    return nonce;
}

BtcE::BtcE(size_t index, QObject *parent) :
    Broker("BtcE", index, parent),
    openOrderInterval(5000),
    sendAccount(false)
{
    connect(&manager, SIGNAL(finished(QNetworkReply*)),
          this, SLOT(replyFinished(QNetworkReply*)));
    QString apiKey = settings.value("apiKey", "insert_apikey_here").toString();
    settings.setValue("apiKey", apiKey);

    uint interval = settings.value("tickerinterval", 30).toUInt();
    interval = qMax(uint(30), interval);
    tickerTimer.setInterval(interval * 1000);
    tickerTimer.setTimerType(Qt::VeryCoarseTimer);
    settings.setValue("tickerinterval", interval);
    connect(&tickerTimer, SIGNAL(timeout()), this, SLOT(getTicker()));
    openOrderTimer.setSingleShot(false);
    connect(&openOrderTimer, SIGNAL(timeout()), this, SLOT(getOpenOrders()));
    openOrderTimer.setTimerType(Qt::VeryCoarseTimer);
}

bool BtcE::startServe()
{
    getRequest("/api/3/info", opPairInfo);
    postRequest(QUrlQuery(), opActiveOrders);
    postRequest(QUrlQuery(), opTradeHistory);
    return true;
}

void BtcE::printTicker(const Ticker &ticker, const TradePair &pair)
{
    QString string("[%2-%3] price:%14.%1f, buy:%14.%1f, sell:%14.%1f");
    string = string.arg(pair.decimal_places)
            .arg(currencyToPlainString(pair.local))
            .arg(currencyToPlainString(pair.remote));
    qDebug(qPrintable(string), ticker.price.price, ticker.price.buy, ticker.price.sell);
}

void BtcE::getTicker()
{
    QString path("/api/3/ticker/");
    QStringList p;
    for (int i = 0; i < m_account.tradeInfo.size(); i++)
    {
        p << QString("%1_%2").arg(currencyToPlainString(m_account.tradeInfo.at(i).remote)).arg(currencyToPlainString(m_account.tradeInfo.at(i).local));
    }
    path += p.join("-").toLower();

    if (!p.empty())
        getRequest(path, opTicker);
}

void BtcE::sendNextPostRequest()
{
    if (networkQueue.empty())
        return;
    if (!networkQueries.empty())
        return;
    PostRequest pr = networkQueue.takeFirst();
    QNetworkReply *reply = manager.post(pr.request, pr.data.query().toUtf8());

//    qDebug() << pr.data.query();
    networkQueries.insert(reply, pr.order);
    networkOperations.insert(reply, pr.operation);
}

void BtcE::postRequest(QUrlQuery postData, Operation o, Order order)
{
    postData.addQueryItem("nonce", QString::number(getNonce()));
    postData.addQueryItem("method", getMethod(o));

    QUrl url(api::baseUrl);
    url.setPath(api::basePath);

//    qDebug() << api::basePath;

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QString apiKey = settings.value("apiKey").toString();
    QString secret = settings.value("secret").toString();
    request.setRawHeader("Key", apiKey.toLocal8Bit());

    QByteArray sign = QMessageAuthenticationCode::hash(postData.query().toLocal8Bit(), secret.toLocal8Bit(), QCryptographicHash::Sha512).toHex();
    request.setRawHeader("Sign", sign);

//    PostRequest pr {request, postData, order, o};
    PostRequest pr;
    pr.request = request;
    pr.data = postData;
    pr.order = order;
    pr.operation = o;

    networkQueue.push_back(pr);
    sendNextPostRequest();
}

void BtcE::getRequest(QString path, Operation o)
{
    QUrl url(api::baseUrl);
    url.setPath(path);

//    qDebug() << path;

    QNetworkRequest request(url);
    QNetworkReply *reply = manager.get(request);
    networkOperations.insert(reply, o);
}

bool readFunds(const QScriptValue &result, Account &a, QList<Currency> blackList)
{
    QScriptValueIterator it(result);
    while (it.hasNext()) {
        it.next();
        Currency c = stringToCurrency(it.name().toUpper().toLocal8Bit());
        if (UndefinedCurrency == c)
            qWarning() << "Unknown currency:" << it.name();
        else if (!blackList.contains(c))
            a.available[c] = it.value().toString().toDouble();
    }
    return true;
}

bool readTransactions(const QScriptValue &result, QList<Order> &/*os*/)
{
    QScriptValueIterator it(result);
    while (it.hasNext()) {
        it.next();

        qDebug() << "TransID:" << it.name();
//        o.id = it.name().toULongLong();
        QScriptValueIterator it2(it.value());
        while (it2.hasNext()) {
            it2.next();
//            if (it2.name() == "pair")
//            {
//                QStringList l = it2.value().toString().split("_");
//                o.amount.currency = stringToCurrency(l.at(0).toUpper().toLocal8Bit());
//                o.price.currency = stringToCurrency(l.at(1).toUpper().toLocal8Bit());
//            }
//            else if (it2.name() == "type")
//                o.type = stringToType(it2.value().toString().toUpper().toLocal8Bit());
//            else if (it2.name() == "amount")
//                o.amount.value = it2.value().toNumber();
//            else if (it2.name() == "rate")
//                o.price.value = it2.value().toNumber();
//            else if (it2.name() == "timestamp_created")
//                o.date = QDateTime::fromMSecsSinceEpoch(it2.value().toString().toULongLong()*1000);
////            else if (it2.name() == "status")
////                o.date = QDateTime::fromMSecsSinceEpoch(it2.value().toString().toULongLong()*1000);
//            else
                qDebug() << "Unknown field2 (readTransactions):" << it2.name() << ": " << it2.value().toString();
        }
    }
    return true;
}

bool readOpenOrders(const QScriptValue &result, QList<Order> &os, const Order order)
{
    QScriptValueIterator it(result);
    while (it.hasNext()) {
        it.next();

        Order o = order;

        o.id = it.name().toULongLong();
        QScriptValueIterator it2(it.value());
        while (it2.hasNext()) {
            it2.next();
            if (it2.name() == "pair")
            {
                QStringList l = it2.value().toString().split("_");
                o.amount.currency = stringToCurrency(l.at(0).toUpper().toLocal8Bit());
                o.price.currency = stringToCurrency(l.at(1).toUpper().toLocal8Bit());
            }
            else if (it2.name() == "type")
                o.type = stringToType(it2.value().toString().toUpper().toLocal8Bit());
            else if (it2.name() == "amount")
                o.amount.value = it2.value().toNumber();
            else if (it2.name() == "rate")
                o.price.value = it2.value().toNumber();
            else if (it2.name() == "timestamp_created")
                o.date = QDateTime::fromMSecsSinceEpoch(it2.value().toString().toULongLong()*1000);
            else if (it2.name() == "status")
            {
                int s = it2.value().toInteger();
                o.status = 0 == s ? Open : UndefinedStatus;
                if (UndefinedStatus == o.status)
                {
                    qDebug() << "Unknown status" << s << "for order: " << o.toString();
                    o.status = Open;
                }
            }
//                o.date = QDateTime::fromMSecsSinceEpoch(it2.value().toString().toULongLong()*1000);
            else
                qDebug() << "Unknown field2 (readOpenOrders):" << it2.name() << ": " << it2.value().toString();
        }

        os << o;
    }
    return true;
}

bool readTrades(const QScriptValue &result, QList<Order> &os, const Order order)
{
    QScriptValueIterator it(result);
    while (it.hasNext()) {
        it.next();

        Order o = order;
        o.status = Filled;
        QScriptValueIterator it2(it.value());
        while (it2.hasNext()) {
            it2.next();
            if (it2.name() == "pair")
            {
                QStringList l = it2.value().toString().split("_");
                o.amount.currency = stringToCurrency(l.at(0).toUpper().toLocal8Bit());
                o.price.currency = stringToCurrency(l.at(1).toUpper().toLocal8Bit());
            }
            else if (it2.name() == "type")
                o.type = stringToType(it2.value().toString().toUpper().toLocal8Bit());
            else if (it2.name() == "amount")
                o.amount.value = it2.value().toNumber();
            else if (it2.name() == "rate")
                o.price.value = it2.value().toNumber();
            else if (it2.name() == "order_id")
                o.id = it2.value().toString().toULongLong();
            else if (it2.name() == "is_your_order") // It reflects who created original order. True means that you opened the order and then someone completely bought/sold it. False means you bought/sold from someone else's order.
            {}
            else if (it2.name() == "timestamp")
                o.date = QDateTime::fromMSecsSinceEpoch(it2.value().toString().toULongLong()*1000);
            else
                qDebug() << "Unknown field2 (readTrades):" << it2.name() << ": " << it2.value().toString();
        }
        os << o;
    }
    return true;
}

bool readTickers(const QScriptValue &result, QList<Ticker> &ts)
{
    QScriptValueIterator it(result);
    while (it.hasNext()) {
        it.next();

        Ticker t;

        QStringList l = it.name().split("_");
        t.remote = stringToCurrency(l.at(0).toUpper().toLocal8Bit());
        t.local = stringToCurrency(l.at(1).toUpper().toLocal8Bit());

        QScriptValueIterator it2(it.value());
        while (it2.hasNext()) {
            it2.next();

            if (it2.name() == "high")
                t.high = it2.value().toNumber();
            else if (it2.name() == "low")
                t.low = it2.value().toNumber();
            else if (it2.name() == "avg")
                t.avg = it2.value().toNumber();
            else if (it2.name() == "vol")
                t.monthVolume = it2.value().toNumber();
            else if (it2.name() == "vol_cur")
                t.volume = it2.value().toNumber();
            else if (it2.name() == "last")
                t.price.price = it2.value().toNumber();
            else if (it2.name() == "buy")
                t.price.buy = it2.value().toNumber();
            else if (it2.name() == "sell")
                t.price.sell = it2.value().toNumber();
            else if (it2.name() == "updated")
                t.date = QDateTime::fromMSecsSinceEpoch(it2.value().toString().toULongLong()*1000);
            else
                qDebug() << "Unknown field2 (readTicker):" << it2.name() << ": " << it2.value().toString();
        }
        ts << t;
    }
    return true;
}

bool readPairInfo(const QScriptValue &result, Account &acc, QList<Currency> blackList)
{
    acc.tradeInfo.clear();

    QScriptValueIterator it(result);
    while (it.hasNext()) {
        it.next();

        TradePair tp;

        QStringList l = it.name().split("_");
        tp.remote = stringToCurrency(l.at(0).toUpper().toLocal8Bit());
        tp.local = stringToCurrency(l.at(1).toUpper().toLocal8Bit());

        if (blackList.contains(tp.remote) || blackList.contains(tp.local))
            continue;

        QScriptValueIterator it2(it.value());
        while (it2.hasNext()) {
            it2.next();
            if (it2.name() == "decimal_places")
                tp.decimal_places = it2.value().toUInt32();
            else if (it2.name() == "min_price")
                tp.min_price = it2.value().toNumber();
            else if (it2.name() == "max_price")
                tp.max_price = it2.value().toNumber();
            else if (it2.name() == "min_amount")
                tp.min_amount = it2.value().toNumber();
            else if (it2.name() == "hidden")
                tp.hidden = it2.value().toInteger() == 1;
            else if (it2.name() == "fee")
                tp.fee = it2.value().toNumber() * 0.01;
            else
                qDebug() << "Unknown field2 (readPairInfo):" << it2.name() << ": " << it2.value().toString();
        }
        acc.tradeTable[tp.local][tp.remote] = acc.tradeInfo.size();
        acc.tradeInfo << tp;
    }

    return true;
}

void BtcE::replyFinished(QNetworkReply *reply)
{
    settings.sync();

    reply->deleteLater();

    Order order;

    if (networkQueries.contains(reply))
        order = networkQueries.take(reply);
    Operation operation = networkOperations.take(reply);
    sendNextPostRequest();

    QStringList blist = settings.value("blacklist").toStringList();
    settings.setValue("blacklist", blist);
    QList<Currency> blackList;
    foreach (const QString &s, blist)
        blackList << stringToCurrency(qPrintable(s));

    QString r(reply->readAll());

//    qDebug() << "Got reply" << r;
    QString mime = reply->header(QNetworkRequest::ContentTypeHeader).toString();
    if (mime.toLower() != "text/html; charset=utf-8")
    {
        qWarning() << "Unhandled response to url" << reply->url().toString() << r;
        qWarning() << mime << reply->errorString();
        return;
    }

    QScriptEngine engine;
    QScriptValue result = engine.evaluate(QString("(%1)").arg(r));

    int success = result.property("success").toInteger();
    QString error = result.property("error").toString();
    QScriptValue ret = result.property("return");

    switch (operation) {
    case opPairInfo:
        readPairInfo(result.property("pairs"), m_account, blackList);
        postRequest(QUrlQuery(), opGetInfo);
        return;
    case opTicker:
    {
        QList<Ticker> ts;
        readTickers(result, ts);
        foreach (const Ticker &t, ts)
            analyzeTicker(t);
        return;
    }
    case opTrade:
        if (success != 1)
        {
            if ((Buy == order.type && error == QString("It is not enough %1 for purchase").arg(currencyToPlainString(order.price.currency)))
                    || (Sell == order.type && error == QString("It is not enough %1 in the account for sale.").arg(currencyToPlainString(order.amount.currency))))
            {
                order.status = InsufficientFunds;
                emit orderStatusChanged(order);
                return;
            }
        } else {
            order.id = ret.property("order_id").toString().toULongLong();
            order.status = ret.property("remains").toNumber() <= 0 ? Filled :
                           ret.property("received").toNumber() > 0 ? PartiallyFilled : Open;
            readFunds(ret.property("funds"), m_account, blackList);

            if (0 == order.id)
//            {
//                emit orderStatusChanged(order);
                postRequest(QUrlQuery(), opTradeHistory, order);
//            }
            else
            {
                postRequest(QUrlQuery(), opActiveOrders, order);

                openOrderInterval = 10000;
                if (!openOrderTimer.isActive() || openOrderTimer.remainingTime() > openOrderInterval)
                    openOrderTimer.start(openOrderInterval);
            }

            emit balance(m_account);
            return;
        }
        return;
    case opCancelOrder:
        if (success == 1)
        {
            order.id = ret.property("order_id").toString().toULongLong();
            order.status = Closed;
            readFunds(ret.property("funds"), m_account, blackList);

            emit balance(m_account);
            emit orderStatusChanged(order);
            return;
        }
        else if (error == "bad status")
            return;
        break;
    case opActiveOrders:
    {
        if (success == 1)
        {
            QList<Order> os;
            readOpenOrders(ret, os, order);

            QHash<quint64, Order> maybeActiveOrders = activeOrders;
            activeOrders.clear();

            foreach (const Order &o, os)
            {
                activeOrders.insert(o.id, o);
                if (!maybeActiveOrders.contains(o.id))
                {
                    maybeActiveOrders.insert(o.id, o);
                    emit orderStatusChanged(o);
                }
            }
            if (maybeActiveOrders.size() > activeOrders.size())
            {
                sendAccount = true;
                postRequest(QUrlQuery(), opTradeHistory, order);
            }

            openOrderInterval *= 1.5;
//            qDebug() << "Setting openOrderTimer to" << openOrderInterval;
            openOrderTimer.start(openOrderInterval);
            return;
        }
        else if (error == "no orders")
        {
            sendAccount = !activeOrders.empty();
            openOrderTimer.stop();
            postRequest(QUrlQuery(), opTradeHistory, order);
            activeOrders.clear();
            return;
        }
    }
    case opTradeHistory:
    {
        if (success == 1)
        {
            QList<Order> os;
            readTrades(ret, os, order);
            foreach (const Order &o, os)
            {
                if (!inactiveOrders.contains(o.id))
                {
                    if (sendAccount)
                    {
                        emit balance(m_account);
                        sendAccount = false;
                    }
                    inactiveOrders.insert(o.id, o);
                    emit orderStatusChanged(o);
                }
                else
                    break;
            }
            return;
        }
    }
    case opTransHistory:
    {
        if (success == 1)
        {
            QList<Order> os;
            readTransactions(ret, os);
            return;
        }
    }
    case opGetInfo:
    {
        if (success == 1)
        {
            readFunds(ret.property("funds"), m_account, blackList);
            emit balance(m_account);
            getTicker();
            tickerTimer.start();
            return;
        }
    }
    default:
        break;
    }

    qWarning() << "ERROR" << mime << reply->errorString() << r;

}

void BtcE::getOpenOrders()
{
    postRequest(QUrlQuery(), opActiveOrders);
}

void BtcE::placeOrder(Order order)
{
    int index = m_account.tradeTable[order.price.currency][order.amount.currency];
    if (index < 0)
    {
        qDebug() << "Invalid currency for broker";
        return;
    }
    const TradePair &tp = m_account.tradeInfo.at(index);

    QUrlQuery q;
    QString pair = QString("%1_%2")
            .arg(currencyToPlainString(order.amount.currency))
            .arg(currencyToPlainString(order.price.currency));
    q.addQueryItem("pair", pair.toLower().toLocal8Bit());
    q.addQueryItem("type", QString(typeToString(order.type)).toLower());
    q.addQueryItem("rate", QString::number(order.price.value, 'g', tp.decimal_places));
    q.addQueryItem("amount", QString::number(order.amount.value, 'g', tp.decimal_places));
    postRequest(q, opTrade, order);
}

void BtcE::closeOrder(Order order)
{
    QUrlQuery q;
    q.addQueryItem("order_id", QString::number(order.id));
    postRequest(q, opCancelOrder, order);
}

void BtcE::getTransactions()
{
    postRequest(QUrlQuery(), opTransHistory);
}

void BtcE::getBalance()
{
    postRequest(QUrlQuery(), opGetInfo);
}

void BtcE::analyzeTicker(const Ticker &ticker)
{
    static Ticker lastTicker[UndefinedCurrency][UndefinedCurrency];

    if (lastTicker[ticker.local][ticker.remote].price.price != ticker.price.price ||
            lastTicker[ticker.local][ticker.remote].price.buy != ticker.price.buy ||
            lastTicker[ticker.local][ticker.remote].price.sell != ticker.price.sell)
    {
//        printTicker(ticker, m_account.tradeInfo.at(m_account.tradeTable[ticker.local][ticker.remote]));

        emit tick(ticker);
    }

    lastTicker[ticker.local][ticker.remote] = ticker;
}

void BtcE::addOptions(std::vector<Broker*> *v, OptionParser *op)
{
    op->add(new Option::Multiple<BtcE*, false, Broker*>(
                v, "btce", 'e', "Adds a BTC-E broker"));
}
const int BtcE::classId = Factory<Broker>::registerClass(&(BtcE::addOptions),
                                                       "btce");

