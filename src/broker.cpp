#include "broker.h"

Broker::Broker(QString name, size_t index, QObject *parent) :
    QObject(parent),
    m_name(name),
    m_index(index)
{
    qDebug("[%s] New Broker.", qPrintable(m_name));
    settings.beginGroup(m_name);
}

bool Broker::claimRead(int classId)
{
    int maxInstances = 1;
    QList<int> &claims = claimedReads;

    if (classId < 0 && claims.size() < maxInstances)
    {
        claims << -1;
        return true;
    }
    else if (classId >= 0 && claims.count(-1) < maxInstances && claims.count(classId) < maxInstances)
    {
        claims << classId;
        return true;
    }
    return false;
}

void Broker::rename(const QString name)
{
    settings.endGroup();
    m_name = name + "_" + m_name;
    settings.beginGroup(m_name);
    qDebug("[%s] reNew Broker.", qPrintable(m_name));
}

bool Broker::claimWrite(int classId)
{
    int maxInstances = 1;
    QList<int> &claims = claimedWrites;

    if (classId < 0 && claims.size() < maxInstances)
    {
        claims << -1;
        return true;
    }
    else if (classId >= 0 && claims.count(-1) < maxInstances && claims.count(classId) < maxInstances)
    {
        claims << classId;
        return true;
    }
    return false;
}
