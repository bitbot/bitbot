#include "bogusbroker.h"

#include "factory.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>

#include <QVariant>

BogusBroker::BogusBroker(size_t index, QObject *parent) :
    Broker("Bogus", index, parent),
    orderDB(NULL),
    databaseName(m_name)
{
}

void BogusBroker::addOptions(std::vector<Broker*> *v, OptionParser *op)
{
    op->add(new Option::Multiple<BogusBroker*, false, Broker*>(
                v, "bogus", 'b', "Adds a Bogus Broker"));
}

bool BogusBroker::connectObjects(std::vector<Broker*> *brokers,
                                 std::vector<Trader*> *, QSqlDatabase *db)
{
    orderDB = db;

    for (unsigned int i = 0; i < brokers->size(); i++)
    {
        if (brokers->at(i)->claimRead(getClassId()))
        {
            QObject::connect(brokers->at(i), SIGNAL(tick(Ticker)),
                             this, SLOT(tickInput(Ticker)));
            rename(brokers->at(i)->name());
            return openDb();
        }
    }

    settings.sync();
    qCritical("[%s] No accessible Broker found.", qPrintable(m_name));
    return false;
}



bool BogusBroker::openDb()
{
    if (!orderDB->tables().contains(databaseName)) {
        QSqlQuery q = orderDB->exec("CREATE TABLE " + databaseName + "("
                              "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                              "type INTEGER,"
                              "status INTEGER,"
                              "time INTEGER,"
                              "amount REAL,"
                              "price REAL,"
                              "amount_currency INTEGER,"
                              "price_currency INTEGER"
                              ");");
        if (q.lastError().isValid())
        {
            qCritical() << "Could not create table:" << orderDB->lastError().text();
            return false;
        }
    }
    return true;
}

void BogusBroker::getOrderDB(Order &order)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM " + databaseName + " WHERE id = ?;");
    query.addBindValue(order.id);

    if (!query.exec()) {
        qCritical() << "Query error1: " << query.lastError().text();
    }
    if (query.next())
    {
        order.type = Type(query.value(1).toInt());
        order.status = Status(query.value(2).toInt());
        order.date = QDateTime::fromMSecsSinceEpoch(query.value(3).toULongLong());
        order.amount.value = query.value(4).toReal();
        order.price.value = query.value(5).toReal();
        order.amount.currency = Currency(query.value(6).toInt());
        order.price.currency = Currency(query.value(7).toInt());
    }
    else
    {
        order.id = -1;
    }
}

qint64 BogusBroker::insertOrderDB(Order &order)
{
    {
        QSqlQuery query;
        query.prepare("INSERT INTO " + databaseName + " ("
                      "type,"
                      "status,"
                      "time,"
                      "amount,"
                      "price,"
                      "amount_currency,"
                      "price_currency"
                      ") VALUES (?, ?, ?, ?, ?, ?, ?);");
        //    query.addBindValue(order.id);
        query.addBindValue((int)order.type);
        query.addBindValue((int)order.status);
        query.addBindValue(order.date.toMSecsSinceEpoch());
        query.addBindValue(order.amount.value);
        query.addBindValue(order.price.value);
        query.addBindValue((int)order.amount.currency);
        query.addBindValue((int)order.price.currency);

        if (!query.exec()) {
            qCritical() << "Query error2:" << query.lastError().text();
        }
    }
    {
        QSqlQuery query;
        query.prepare("SELECT last_insert_rowid();");

        if (!query.exec()) {
            qCritical() << "Query error22:" << query.lastError().text();
        }
        if (query.next())
        {
            return query.value(0).toInt();
        }
    }
    return -1;
}

void BogusBroker::setOrderStatusDB(Order &order, Status s)
{
    QSqlQuery query;
    query.prepare("UPDATE " + databaseName + " SET status = ? WHERE id = ?;");
    query.addBindValue(s);
    query.addBindValue(order.id);

    if (!query.exec()) {
        qCritical() << "Query error3:" << query.lastError().text();
    }
}

QList<qint64> BogusBroker::getMaxMinOrderDB(Price &p)
{
    QList<qint64> ids;

    {
        QSqlQuery query;
        query.prepare("SELECT id FROM " + databaseName + " WHERE price <= ? AND type = ? AND status = ? ORDER BY price ASC;");
//        query.addBindValue(1000);
        query.addBindValue(p.buy);
        query.addBindValue((int)Sell);
        query.addBindValue((int)Open);

        if (!query.exec()) {
            qCritical() << "Query error1: " << query.lastError().text();
        }
        while (query.next())
        {
            ids << query.value(0).toInt();
//            qDebug() << "POOP4a" << query.value(5).toReal();
        }
    }
    {
        QSqlQuery query;
        query.prepare("SELECT id FROM " + databaseName + " WHERE price >= ? AND type = ? AND status = ? ORDER BY price DESC;");
//        query.addBindValue(1);
        query.addBindValue(p.sell);
        query.addBindValue((int)Buy);
        query.addBindValue((int)Open);

        if (!query.exec()) {
            qCritical() << "Query error1: " << query.lastError().text();
        }
        while (query.next())
        {
            ids << query.value(0).toInt();
//            qDebug() << "POOP4b" << query.value(5).toReal();
        }
    }
    return ids;
}

QList<Order> BogusBroker::getOpenOrdersDB()
{
    QList<Order> openOrders;

    QSqlQuery query;
    query.prepare("SELECT * FROM " + databaseName + " WHERE status = ? ORDER BY id ASC;");
    query.addBindValue((int)Open);

    if (!query.exec()) {
        qCritical() << "Query error1: " << query.lastError().text();
    }
    while (query.next())
    {
        Order order;
        order.type = Type(query.value(1).toInt());
        order.status = Status(query.value(2).toInt());
        order.date = QDateTime::fromMSecsSinceEpoch(query.value(3).toULongLong());
        order.amount.value = query.value(4).toReal();
        order.price.value = query.value(5).toReal();
        order.amount.currency = Currency(query.value(6).toInt());
        order.price.currency = Currency(query.value(7).toInt());

        openOrders << order;
    }
    return openOrders;
}

void BogusBroker::getAccountAvailable(Account &account)
{
    QList<Order> orders = getOpenOrdersDB();

    for (int i = 0; i < UndefinedCurrency; i++)
        account.available[i] = account.total[i];

    for (int i = 0; i < orders.size(); ++i)
    {
        const Order &order = orders.at(i);

        if (Buy == order.type)
        {
            qreal orderValue = order.amount.value * order.price.value;
            account.available[order.price.currency] -= orderValue;
        }
        else if (Sell == order.type)
        {
            qreal orderValue = order.amount.value;
            account.available[order.amount.currency] -= orderValue;
        }
    }
}








bool BogusBroker::startServe()
{
    getBalance();
    getOpenOrders();

    return true;
}

void BogusBroker::placeOrder(Order order)
{
    Account account = readAccount();

//    qint64 orderId = settings.value("orderId", 0).toLongLong();
//    order.id = orderId;
//    settings.setValue("orderId", orderId + 1);

    order.date = QDateTime::currentDateTime();

    if (account.available[order.price.currency] < 0 ||
            account.available[order.amount.currency] < 0)
    {
        order.status = Broken;
        queryOrder(order);
        return;
    }

    order.status = UndefinedStatus;

    if (order.type == Sell)
    {
        qreal orderValue = order.amount.value;
        if (orderValue > account.available[order.amount.currency])
            order.status = InsufficientFunds;
        else if (orderValue > 0)
        {
            order.status = Open;
            account.available[order.amount.currency] -= orderValue;
        }
    }
    else if (order.type == Buy)
    {
        qreal orderValue = order.amount.value * order.price.value;

        if (orderValue > account.available[order.price.currency])
            order.status = InsufficientFunds;
        else if (orderValue > 0)
        {
            order.status = Open;
            account.available[order.price.currency] -= orderValue;
        }
    }

    writeAccount(account);

//    orders << order;
    order.id = insertOrderDB(order);
    queryOrder(order);
}

void BogusBroker::closeOrder(Order order)
{
    Account account = readAccount();

    setOrderStatusDB(order, Closed);

    getOrderDB(order);

    if (order.type == Sell)
    {
        qreal orderValue = order.amount.value;
        if (orderValue > 0)
        {
            account.available[order.amount.currency] += orderValue;
        }
    }
    else if (order.type == Buy)
    {
        qreal orderValue = order.amount.value * order.price.value;

        if (orderValue > 0)
        {
            account.available[order.price.currency] += orderValue;
        }
    }
    writeAccount(account);

    queryOrder(order);
}

void BogusBroker::queryOrder(Order order)
{
    getOrderDB(order);
    emit orderStatusChanged(order);
}

void BogusBroker::getOpenOrders()
{
    QList<Order> orders = getOpenOrdersDB();
//    emit openOrders(orders);
}

void BogusBroker::getBalance()
{
    Account account = readAccount();
    emit balance(account);
    writeAccount(account);
}

void BogusBroker::tickInput(Ticker ticker)
{

    QList<qint64> ids = getMaxMinOrderDB(ticker.price);

//    if (ids.size())
//        qDebug() << "Open orders:" << ids;

    foreach (qint64 id, ids)
    {
        Order o;
        o.id = id;
        fillOrder(o);
    }

//    getMaxMinPriceDB(ticker.p);

    emit tick(ticker);
}

void BogusBroker::fillOrder(Order order)
{
    Account account = readAccount();
    
    getOrderDB(order);

    const TradePair t = account.tradeInfo.at(account.tradeTable[order.price.currency][order.amount.currency]);
    
    if (order.type == Sell)
    {
        qreal orderValue = order.amount.value;
        if (orderValue > 0)
        {
            setOrderStatusDB(order, Filled);
            account.total[order.amount.currency] -= orderValue;
            account.total[order.price.currency] += order.amount.value * order.price.value * qreal(1.0 - t.fee);
        }
    }
    else if (order.type == Buy)
    {
        qreal orderValue = order.amount.value * order.price.value;
        
        if (orderValue > 0)
        {
            setOrderStatusDB(order, Filled);
            account.total[order.price.currency] -= orderValue;
            account.total[order.amount.currency] += order.amount.value * qreal(1.0 - t.fee);
        }
    }
    writeAccount(account);
    
    order.date = QDateTime::currentDateTime();
    
//    queryOrder(order);
}

Account BogusBroker::readAccount()
{
    settings.sync();
    Account account;

    TradePair p;
    p.hidden = true;
    p.decimal_places = 8;
    p.max_price = 100000;
    p.min_price = .000001;
    p.min_amount = .000001;

    for (int i = 0; i < UndefinedCurrency; i++)
        for (int j = 0; j < UndefinedCurrency; j++)
            if (i != j)
            {
                p.local = Currency(i);
                p.remote = Currency(j);
                p.fee = settings.value(QString("fee.%1.%2").arg(currencyToPlainString(Currency(i))).arg(currencyToPlainString(Currency(j))), .0095).toReal();

                account.tradeTable[p.local][p.remote] = account.tradeInfo.size();
                account.tradeInfo << p;
            }

    for (int i = 0; i < UndefinedCurrency; i++)
        account.total[i] = settings.value(QString("total.") + currencyToPlainString(Currency(i)), -1).toReal();

    getAccountAvailable(account);

    bool t(false);
    for (int i = 0; i < UndefinedCurrency; i++)
    {
        t = t || account.total[i] > 0;
    }
    if (!t)
    {
        qWarning() << "No value in any currency defined in BogusBroker! Change config file!";
    }

    return account;
}

void BogusBroker::writeAccount(Account account)
{
    foreach (const TradePair &tp, account.tradeInfo)
        settings.setValue(QString("fee.%1.%2").arg(currencyToPlainString(tp.local)).arg(currencyToPlainString(tp.remote)), tp.fee);

    for (int i = 0; i < UndefinedCurrency; i++)
        settings.setValue(QString("total.") + currencyToPlainString(Currency(i)), account.total[i]);

//    settings.sync();
}

const int BogusBroker::classId = Factory<Broker>::registerClass(
            &(BogusBroker::addOptions), "bogus");


