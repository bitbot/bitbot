#include "simpletrader.h"

#include "factory.h"

#include <QTimer>

SimpleTrader::SimpleTrader(size_t index, QObject *parent) :
    Trader("ST", index, parent)
{
    for (int i = 0; i < 2; ++i)
        for (int l = 0; l < UndefinedCurrency; l++)
            for (int r = 0; r < UndefinedCurrency; r++)
                lastOrderOfType[l][r][i].status = Broken;
}

bool SimpleTrader::connectObjects(std::vector<Broker*> *brokers, std::vector<Trader*>*, QSqlDatabase *)
{
    for (unsigned int i = 0; i < brokers->size(); i++)
    {
        Broker* b = brokers->at(i);

        if (b->claimWrite(getClassId()))
        {
            QObject::connect(b, SIGNAL(tick(Ticker)),
                             this, SLOT(analyzeTicker(Ticker)));
            QObject::connect(b, SIGNAL(orderStatusChanged(Order)),
                             this, SLOT(getOrder(Order)));
            QObject::connect(b, SIGNAL(balance(Account)),
                             this, SLOT(setBalance(Account)));

            QObject::connect(this, SIGNAL(placeOrder(Order)),
                             b, SLOT(placeOrder(Order)));
            QObject::connect(this, SIGNAL(closeOrder(Order)),
                             b, SLOT(closeOrder(Order)));
            rename(b->name());
            return true;
        }
    }
    qCritical("[%s] No accessible Broker found.", qPrintable(m_name));
    return false;
}

void SimpleTrader::analyzeTicker(Ticker ticker)
{
    settings.sync();

    lastTickers[ticker.local][ticker.remote] = ticker;

    Order *last = lastOrderOfType[ticker.local][ticker.remote];

    /**
     * Check last order
     */
    for (int i = 0; i < 2; i++)
    {
        if (last[i].status == Broken) // Fake a last order of type
        {
            Order o;
            o.originatingTicker = m_name;
            o.type = UndefinedType;
            o.amount.currency = ticker.remote;
            o.price.value = i ? ticker.price.sell : ticker.price.buy;
            o.price.currency = ticker.local;
            o.status = Filled;
            o.date = QDateTime::fromMSecsSinceEpoch(0);
            o.id = 0;

            o.price.value = settings.value(
                        QString("lastPrice.%1.%2")
                        .arg(currencyToPlainString(ticker.local))
                        .arg(currencyToPlainString(ticker.remote)),
                        o.price.value).toReal();
            settings.setValue(
                        QString("lastPrice.%1.%2")
                        .arg(currencyToPlainString(ticker.local))
                        .arg(currencyToPlainString(ticker.remote)),
                        o.price.value);

            last[i] = o;
        }
        else if (last[i].status != Filled)
        {
            qCritical("Error, last order is broken: %s", qPrintable(last[i].toString()));
            return;
        }
    }

    /**
     * Read settings
     */
    const Order lastOrder = last[Buy].id > last[Sell].id ? last[Buy] : last[Sell];

    Price percent;
    percent.price = (ticker.price.price / lastOrder.price.value) - qreal(1);
    percent.buy   = (ticker.price.buy   / lastOrder.price.value) - qreal(1);
    percent.sell  = (ticker.price.sell  / lastOrder.price.value) - qreal(1);

    qreal threshold = qMax(qreal(0), settings.value("threshold", .02).toReal());
    settings.setValue("threshold", threshold);
    qreal percentMultiplier = settings.value("percentMultiplier", 1).toReal();
    settings.setValue("percentMultiplier", percentMultiplier);
    qreal maxPercent = settings.value("maxPercent", .1).toReal();
    settings.setValue("maxPercent", maxPercent);


    /**
     * Create next order
     */
    Order nextOrder;
    nextOrder.originatingTicker = m_name;
    nextOrder.type = percent.sell < -threshold ? Buy :
                     percent.buy > threshold ? Sell :
                     UndefinedType;

    if (nextOrder.type == UndefinedType)
        return;

    nextOrder.price.currency = ticker.local;
    nextOrder.amount.currency = ticker.remote;

    nextOrder.price.value = nextOrder.type == Sell ? ticker.price.buy : ticker.price.sell;

    if (nextOrder.type == Sell)
    {
        qreal share = percent.buy;
        share = qMin(share * percentMultiplier, maxPercent);

        nextOrder.amount.value = currentAccount.available[ticker.remote] * share;
    }
    else
    {
        qreal share = -percent.sell;
        share = qMin(share * percentMultiplier, maxPercent);

        nextOrder.amount.value = currentAccount.available[ticker.local] * share;
        nextOrder.amount.value = nextOrder.amount.value / ticker.price.sell;
    }

    /**
     * Check limitations
     */
    int index = currentAccount.tradeTable[ticker.local][ticker.remote];
    if (index < 0)
        return;

    const TradePair &tp = currentAccount.tradeInfo.at(index);

    if (nextOrder.amount.value <= 0)
        return;

    if (tp.max_price < nextOrder.price.value || tp.min_price > nextOrder.price.value || tp.min_amount > nextOrder.amount.value)
        return;

//    // Check with average
//    if (Buy == nextOrder.type && ticker.avg > 0 && ticker.avg > nextOrder.price.value)
//        return;
//    if (Sell == nextOrder.type && ticker.avg > 0 && ticker.avg < nextOrder.price.value)
//        return;

    /**
     * Check already open orders
     */
    foreach (const Order &o, m_openOrders)
    {
        if (o.price.currency == nextOrder.price.currency && o.amount.currency == nextOrder.amount.currency && o.type == nextOrder.type)
        {
//            if (o.type == Buy && o.price.value < nextOrder.price.value)
//                return;
//            if (o.type == Sell && o.price.value > nextOrder.price.value)
//                return;
//            emit closeOrder(o);
            return;
        }
    }

//    qDebug() << "Placing order: " << nextOrder.toString();
    emit placeOrder(nextOrder);
}

void SimpleTrader::setBalance(Account balance)
{
    currentAccount = balance;

    qDebug("[%s] %s", qPrintable(m_name), qPrintable(balance.toString()));
}

void SimpleTrader::getOrder(Order order)
{
    if (order.status == Open || order.status == PartiallyFilled)
    {
        foreach (const Order &o, m_openOrders)
        {
            if (o.price.currency == order.price.currency && o.amount.currency == order.amount.currency && o.type == order.type)
            {
                if ((o.type == Buy && o.price.value < order.price.value) || (o.type == Sell && o.price.value > order.price.value))
                    emit closeOrder(order);
                else
                    emit closeOrder(o);
                break;
            }
        }
        m_openOrders.insert(order.id, order);
        QTimer::singleShot(660000, Qt::VeryCoarseTimer, this, SLOT(closeOrders()));
    }
    else
    {
        m_openOrders.remove(order.id);
    }
    qDebug("[%s] %s", qPrintable(m_name), qPrintable(order.toString()));

    Order *last = lastOrderOfType[order.price.currency][order.amount.currency];

    if (order.status == Filled && (last[order.type].id < order.id || last[order.type].status == Broken))
    {
        last[order.type] = order;
        settings.setValue(
                    QString("lastPrice.%1.%2")
                    .arg(currencyToPlainString(order.price.currency))
                    .arg(currencyToPlainString(order.amount.currency)),
                    order.price.value);
    }
    else if (order.status == Closed)
    {
        analyzeTicker(lastTickers[order.price.currency][order.amount.currency]);
    }
}

void SimpleTrader::closeOrders()
{
//    static QDateTime previousCulling = QDateTime::fromMSecsSinceEpoch(0);

    QDateTime now = QDateTime::currentDateTime();
//    if (previousCulling.secsTo(now) < 600)
//        QTimer::singleShot(600 - previousCulling.secsTo(now), Qt::VeryCoarseTimer,
//                           this, SLOT(closeOrders()));
//    else
        foreach (const Order &o, m_openOrders)
        {
            if (o.date.secsTo(now) > 600)
                emit closeOrder(o);
        }
//    previousCulling = now;
}

void SimpleTrader::addOptions(std::vector<Trader*> *v, OptionParser *op)
{
    op->add(new Option::Multiple<SimpleTrader*, false, Trader*>(
                v, "simpletrader", 's', "Adds a stupid cowardly trader"));
}
const int SimpleTrader::classId = Factory<Trader>::registerClass(
            &(SimpleTrader::addOptions), "simpletrader");
