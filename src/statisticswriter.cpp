#include "statisticswriter.h"

#include "factory.h"

#include <QFile>
#include <QDir>

#if QT_VERSION < 0x050000
#include <QDesktopServices>
#else
#include <QStandardPaths>
#endif

StatisticsWriter::StatisticsWriter(size_t index, QObject *parent) :
    Trader("SW", index, parent),
    gotTicker(false),
    gotAccount(false)
{
}

bool StatisticsWriter::connectObjects(std::vector<Broker *> *brokers,
                                      std::vector<Trader *> *, QSqlDatabase *)
{
    for (unsigned int i = 0; i < brokers->size(); i++)
    {
        if (brokers->at(i)->claimRead(getClassId()))
        {
            QObject::connect(brokers->at(i), SIGNAL(balance(Account)),
                             this, SLOT(balance(Account)));
            QObject::connect(brokers->at(i), SIGNAL(tick(Ticker)),
                             this, SLOT(ticker(Ticker)));
            rename(brokers->at(i)->name());
            return true;
        }
    }
    qCritical("[%s] No accessible Broker found.", qPrintable(m_name));
    return false;
}

void StatisticsWriter::addOptions(std::vector<Trader *> *v, OptionParser *op)
{
    op->add(new Option::Multiple<StatisticsWriter*, false, Trader*>(
                v, "statisticswriter", 'w', "Adds a thingy that writes statistics"));
}

void StatisticsWriter::ticker(Ticker ticker)
{
    gotTicker = true;

    m_tickers[ticker.local][ticker.remote] = ticker;
        writeStatistics2();
}

void StatisticsWriter::balance(Account account)
{
    m_account = account;
    gotAccount = true;
    if (gotTicker)
        writeStatistics2();
}

bool openFile(QFile *file, QString name)
{
#if QT_VERSION < 0x050000
    QDir dir(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#else
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
#endif
    dir.mkpath(".");
    QString path(dir.filePath(name));

    file->setFileName(path);

    file->setPermissions(
                QFile::WriteUser |
                QFile::ReadOwner |
                QFile::ReadUser  |
                QFile::ReadGroup |
                QFile::ReadOther |
                QFile::ExeUser   |
                QFile::ExeGroup  |
                QFile::ExeOther  );

    if (!file->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qCritical() << "Could not open file:" << file->errorString();
        return false;
    }

    return true;
}

void StatisticsWriter::convertToCurrency(const Currency local, const Account &account, Account &localAccount) const
{
    for (size_t i = 0; i < UndefinedCurrency; i++)
    {
        if (account.available[i] < 0)
        {
//            localAccount.available[i] = 0;
        }
        else
        {
            if (BTC == i || local == i)
                localAccount.available[i] = account.available[i];
            else
            {
                signed char ind = account.tradeTable[i][BTC];
                if (ind < 0)
                    ind = account.tradeTable[BTC][i];
                if (ind < 0)
                    continue;

                const TradePair toBtc = account.tradeInfo.at(ind);

                if (m_tickers[toBtc.local][toBtc.remote].local == UndefinedCurrency)
                    continue;

                if (toBtc.local == BTC)
                    localAccount.available[i] = account.available[i] * m_tickers[toBtc.local][toBtc.remote].price.buy * qreal(1.0 - toBtc.fee);
                else
                    localAccount.available[i] = account.available[i] / m_tickers[toBtc.local][toBtc.remote].price.sell * qreal(1.0 - toBtc.fee);
            }
        }
    }

//    for (size_t i = 0; i < UndefinedCurrency; i++)
//        qDebug() << localAccount.available[i];

    const TradePair *toLocal = NULL;

    if (local != UndefinedCurrency && local != BTC)
    {
        signed char i = account.tradeTable[local][BTC];
        if (i < 0)
            i = account.tradeTable[BTC][local];
        if (i >= 0)
        {
            toLocal = &account.tradeInfo.at(i);

            for (size_t i = 0; i < UndefinedCurrency; i++)
            {
                if (localAccount.available[i] < 0 || local == i)
                    continue;
                if (toLocal->local == BTC)
                    localAccount.available[i] = localAccount.available[i] / m_tickers[toLocal->local][toLocal->remote].price.sell * qreal(1.0 - toLocal->fee);
                else
                    localAccount.available[i] = localAccount.available[i] * m_tickers[toLocal->local][toLocal->remote].price.buy * qreal(1.0 - toLocal->fee);
            }
        }
    }
}

void StatisticsWriter::writeStatistics2()
{
    settings.sync();
    const Currency local = stringToCurrency(qPrintable(settings.value("localcurrency", "BTC").toString()));
    settings.setValue("localcurrency", settings.value("localcurrency", "BTC").toString());

    Account currentAccount, originalAccount;

    convertToCurrency(local, m_account, currentAccount);

//    for (size_t i = 0; i < UndefinedCurrency; i++)
//        qDebug() << "IN" << currencyToPlainString(local) << ":"  << currencyToPlainString(Currency(i)) << currentAccount.available[i] << m_account.available[i];

    originalAccount = m_account;
    for (int i = 0; i < UndefinedCurrency; i++)
        originalAccount.available[i] = settings.value(QString("original.") + currencyToPlainString(Currency(i)), m_account.available[i]).toReal();
    for (int i = 0; i < UndefinedCurrency; i++)
        settings.setValue(QString("original.") + currencyToPlainString(Currency(i)), originalAccount.available[i]);

    convertToCurrency(local, originalAccount, originalAccount);
    qreal originalTotal = 0;
    for (size_t i = 0; i < UndefinedCurrency; i++)
        if (originalAccount.available[i] > 0)
            originalTotal += originalAccount.available[i];

    QFile file;
    if (openFile(&file, "stats2.sh"))
    {
//        qDebug() << file.fileName();
        QStringList s;
        s << "#!/bin/sh"
          << "user=${0##*/bitbot_munin_}"
          << "if [ \"$1\" = \"config\" ]"
          << "then"
          << "echo \"graph_title Bitcoin value for $user\""
          << "cat <<EOMEOM"
          << "graph_args --base 1000 -l 0"
          << QString("graph_vlabel %1").arg(currencyToPlainString(local));

        int count = 0;
        for (size_t i = 0; i < UndefinedCurrency; i++)
        {
            if (m_account.available[i] < 0)
                continue;
            s << QString("current%1.label Current %1").arg(currencyToPlainString(Currency(i)));
            if (0 == count++)
                s << QString("current%1.draw AREA").arg(currencyToPlainString(Currency(i)));
            else
                s << QString("current%1.draw STACK").arg(currencyToPlainString(Currency(i)));
        }
        s << "total.label Current Total";
        s << "original.label Without trading";
        s << "EOMEOM"
          << "else"
          << "cat <<EOMEOM";

        qreal total(0);
        for (size_t i = 0; i < UndefinedCurrency; i++)
        {
            if (m_account.available[i] < 0)
                continue;
            s << QString("current%1.value %2").arg(currencyToPlainString(Currency(i))).arg(currentAccount.available[i]);
            total += currentAccount.available[i];
        }
        s << QString("total.value %1").arg(total);
        s << QString("original.value %1").arg(originalTotal);
        s << "EOMEOM"
          << "fi";

        QTextStream stream(&file);
        stream << s.join("\n") << endl;

        file.close();
    }
}

//void StatisticsWriter::writeStatistics()
//{
//    qreal original[UndefinedCurrency];

//    for (int i = 0; i < UndefinedCurrency; i++)
//        original[i] = settings.value(QString("original.") + currencyToPlainString(Currency(i)), m_account.total[i]).toReal();
//    for (int i = 0; i < UndefinedCurrency; i++)
//        settings.setValue(QString("original.") + currencyToPlainString(Currency(i)), original[i]);

//    const TradePair t = m_account.tradeInfo.at(m_account.tradeTable[m_ticker.local][m_ticker.remote]);

//    qreal originalPrice = settings.value("original.price", m_ticker.price.buy).toReal();
//    settings.setValue("original.price", originalPrice);

//    qreal originalValue = original[m_ticker.local] + original[m_ticker.remote] * originalPrice * (1 - t.fee);

//    qreal originalValueNow = original[m_ticker.local] + original[m_ticker.remote] * m_ticker.price.buy * (1 - t.fee);

//    qreal currentValueLocal = m_account.total[m_ticker.local];
//    qreal currentValueRemote = m_account.total[m_ticker.remote] * m_ticker.price.buy * (1 - t.fee);
//    qreal currentValue = currentValueLocal + currentValueRemote;


//    QFile file;
//    if (openFile(&file, "stats.txt"))
//    {
//        QString s("%1 %2 %3 %4 %5\n");
//        s = s.arg(originalValue).arg(originalValueNow).arg(currentValueLocal).arg(currentValueRemote).arg(currentValue);
//        file.write(qPrintable(s));
//    }
//}

const int StatisticsWriter::classId = Factory<Trader>::registerClass(&(StatisticsWriter::addOptions),
                                                        "statisticswriter");
