#include "kapiton.h"

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

#if QT_VERSION >= 0x050000
#include <QUrlQuery>
#endif

#include "factory.h"

namespace api {
static const char *baseUrl = "https://kapiton.se";

static const char *getTicker = "/api/0/ticker";
//static const char *getTrades = "/api/0/trades";
//static const char *getOrderBook = "/api/0/orderbook";
static const char *getUserInfo = "/api/0/auth/getuserinfo";
static const char *getOrders = "/api/0/auth/getorders";
static const char *queryOrder = "/api/0/auth/getorder";
static const char *placeOrder = "/api/0/auth/placeorder";
static const char *closeOrder = "/api/0/auth/closeorder";
//static const char *withdrawBtc = "/api/0/auth/withdrawbtc";
}

Kapiton::Kapiton(size_t index, QObject *parent) :
    Broker("Kptn", index, parent),
    openOrderInterval(5000)
{
    connect(&manager, SIGNAL(finished(QNetworkReply*)),
          this, SLOT(replyFinished(QNetworkReply*)));
    QString apiKey = settings.value("apiKey", "insert_apikey_here").toString();
    settings.setValue("apiKey", apiKey);

    uint interval = settings.value("tickerinterval", 30).toUInt();
    interval = qMax(uint(30), interval);
    tickerTimer.setInterval(interval * 1000);
    settings.setValue("tickerinterval", interval);
    connect(&tickerTimer, SIGNAL(timeout()), this, SLOT(getTicker()));
}

bool Kapiton::startServe()
{
//    tickerTimer.start();
    getBalance();
    getOpenOrders();

    QStringList pastOrders = settings.value("pastOrders").toStringList();
    settings.setValue("pastOrders", pastOrders);
    foreach (const QString &s, pastOrders)
    {
        Order o;
        o.id = s.toULongLong();
        queryOrder(o);
    }

    return true;
}

void Kapiton::printTicker(const Ticker &ticker, const TradePair &pair)
{
    QString string("[%2-%3] price:%14.%1f, buy:%14.%1f, sell:%14.%1f");
    string = string.arg(pair.decimal_places)
            .arg(currencyToPlainString(pair.local))
            .arg(currencyToPlainString(pair.remote));
    qDebug(qPrintable(string), ticker.price.price, ticker.price.buy, ticker.price.sell);
}






void Kapiton::getTicker()
{
    getRequest(api::getTicker);
}

//void Kapiton::getTrades(qint64 since)
//{
//    getRequest(api::getTrades, QString::number(since));
//}

//void Kapiton::getOrderBook()
//{
//    getRequest(api::getOrderBook);
//}

void Kapiton::getUserInfo()
{
#if QT_VERSION < 0x050000
    QUrl q;
#else
    QUrlQuery q;
#endif
    QString apiKey = settings.value("apiKey").toString();
    q.addQueryItem("api_key", apiKey);
    settings.setValue("apiKey", apiKey);
#if QT_VERSION < 0x050000
    QString postData = q.encodedQuery();
#else
    QString postData = q.query();
#endif
    postRequest(api::getUserInfo, postData);
}

//void Kapiton::withdrawBtc(qreal amount, QString address)
//{
//#if QT_VERSION < 0x050000
//    QUrl q;
//#else
//    QUrlQuery q;
//#endif
//    q.addQueryItem("amount", QString::number(amount, 'g', 16));
//    q.addQueryItem("address", address);
//    QString apiKey = settings.value("apiKey").toString();
//    q.addQueryItem("api_key", apiKey);
//    settings.setValue("apiKey", apiKey);
//#if QT_VERSION < 0x050000
//    QString postData = q.encodedQuery();
//#else
//    QString postData = q.query();
//#endif
//    postRequest(api::withdrawBtc, postData);
//}





void Kapiton::postRequest(QString path, QString postData, Order *order)
{
    QUrl url(api::baseUrl);
    url.setPath(path);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *reply = manager.post(request, postData.toUtf8());

    if (order != NULL)
        networkQueries.insert(reply, *order);
}

void Kapiton::getRequest(QString path, QString since)
{
    QUrl url(api::baseUrl);
    url.setPath(path);
    if (since.length() > 0)
    {
#if QT_VERSION < 0x050000
        url.addQueryItem("since", since);
#else
        QUrlQuery q;
        q.addQueryItem("since", since);
        url.setQuery(q);
#endif
    }

    QNetworkRequest request(url);
    manager.get(request);
}

bool Kapiton::parseTicker(QScriptValue *result, Ticker &ticker)
{
    if (!result->property("price").isNumber()) {
        qCritical() << "userinfo field price is bad:"
                    << result->property("price").toString();
        return false;
    }
    if (!result->property("bid").isNumber()) {
        qCritical() << "userinfo field bid is bad:"
                    << result->property("bid").toString();
        return false;
    }
    if (!result->property("ask").isNumber()) {
        qCritical() << "userinfo field ask is bad:"
                    << result->property("ask").toString();
        return false;
    }
    if (!result->property("hi").isNumber()) {
        qCritical() << "userinfo field hi is bad:"
                    << result->property("hi").toString();
        return false;
    }
    if (!result->property("low").isNumber()) {
        qCritical() << "userinfo field low is bad:"
                    << result->property("low").toString();
        return false;
    }
    if (!result->property("vol").isNumber()) {
        qCritical() << "userinfo field vol is bad:"
                    << result->property("vol").toString();
        return false;
    }
    if (!result->property("moavg").isNumber()) {
        qCritical() << "userinfo field moavg is bad:"
                    << result->property("moavg").toString();
        return false;
    }
    if (!result->property("movol").isNumber()) {
        qCritical() << "userinfo field low is bad:"
                    << result->property("movol").toString();
        return false;
    }
    ticker.price.price = result->property("price").toNumber();
    ticker.price.buy = result->property("bid").toNumber();
    ticker.price.sell = result->property("ask").toNumber();
    ticker.high = result->property("hi").toNumber();
    ticker.low = result->property("low").toNumber();
    ticker.volume = result->property("vol").toNumber();
    ticker.monthAvg = result->property("moavg").toNumber();
    ticker.monthVolume = result->property("movol").toNumber();
    ticker.date = QDateTime::currentDateTime();
    ticker.remote = BTC;
    ticker.local = SEK;

    return true;
}

bool Kapiton::parseUserInfo(QScriptValue *result, Account &userInfo)
{
    if (!result->property("fee").isNumber()) {
        qCritical() << "userinfo field fee is bad:"
                    << result->property("fee").toString();
        return false;
    }
    if (!result->property("available_btc").isNumber()) {
        qCritical() << "userinfo field availableBtc is bad:"
                    << result->property("available_btc").toString();
        return false;
    }
    if (!result->property("available_sek").isNumber()) {
        qCritical() << "userinfo field availableSek is bad:"
                    << result->property("available_sek").toString();
        return false;
    }
    if (!result->property("total_btc").isNumber()) {
        qCritical() << "userinfo field totalBtc is bad:"
                    << result->property("total_btc").toString();
        return false;
    }
    if (!result->property("total_sek").isNumber()) {
        qCritical() << "userinfo field totalSek is bad:"
                    << result->property("total_sek").toString();
        return false;
    }

    m_pair.fee = result->property("fee").toNumber();
    m_pair.local = SEK;
    m_pair.remote = BTC;
    m_pair.hidden = false;
    m_pair.decimal_places = 8;
    m_pair.max_price = 10000;
    m_pair.min_price = 1;
    m_pair.min_amount = .0001;

    userInfo.tradeInfo.push_back(m_pair);
    userInfo.tradeTable[SEK][BTC] = 0;
    userInfo.available[BTC] = result->property("available_btc").toNumber();
    userInfo.available[SEK] = result->property("available_sek").toNumber();
    userInfo.total[BTC] = result->property("total_btc").toNumber();
    userInfo.total[SEK] = result->property("total_sek").toNumber();

    return true;
}

bool Kapiton::parseOrder(QScriptValue *result, Order &order)
{
    if (!result->property("id").isNumber()) {
        qCritical() << "userinfo field id is bad:"
                    << result->property("id").toString();
        return false;
    }
    if (!result->property("amount").isNumber()) {
        qCritical() << "userinfo field amount is bad:"
                    << result->property("amount").toString();
        return false;
    }
    if (!result->property("price").isNumber()) {
        qCritical() << "userinfo field price is bad:"
                    << result->property("price").toString();
        return false;
    }
    if (!result->property("time").isNumber()) {
        qCritical() << "userinfo field time is bad:"
                    << result->property("time").toString();
        return false;
    }
    if (!result->property("type").isString()) {
        qCritical() << "userinfo field type is bad:"
                    << result->property("type").toString();
        return false;
    }
    if (!result->property("status").isString()) {
        qCritical() << "userinfo field status is bad:"
                    << result->property("status").toString();
        return false;
    }
    order.id = result->property("id").toUInt32();
    order.amount.value = result->property("amount").toNumber();
    order.amount.currency = BTC;
    order.price.value = result->property("price").toNumber();
    order.price.currency = SEK;
    order.date = QDateTime::fromMSecsSinceEpoch(
                quint64(result->property("time").toUInt32()) * 1000);

    QString s = result->property("type").toString();
    order.type = (s == "BID" ? Buy : s == "ASK" ? Sell : UndefinedType);
    if (order.type == UndefinedType)
    {
        qCritical() << "Unknown order type:" << s;
        return false;
    }

    s = result->property("status").toString();
    order.status = (
                s == "CLSD" ? Closed :
                s == "OPEN" ? Open :
                s == "FILL" ? Filled :
                s == "PRTF" ? PartiallyFilled :
                UndefinedStatus);
    if (order.status == UndefinedStatus)
    {
        qCritical() << "Unknown order status:" << s;
        return false;
    }
    return true;
}

void Kapiton::replyFinished(QNetworkReply *reply)
{
    reply->deleteLater();

    QString r(reply->readAll());

    QString mime = reply->header(QNetworkRequest::ContentTypeHeader).toString();
    if (mime != "application/json")
    {
//        qWarning() << "Unhandled response to url" << reply->url().toString();
        qWarning() << reply->errorString();
        return;
    }

    QScriptEngine engine;
    QScriptValue result = engine.evaluate(QString("(%1)").arg(r));

    QString error = result.property("error").toString();

    if (!error.isEmpty())
    {
        if (error == "Insufficient funds")
        {
            Order order = networkQueries.take(reply);

            order.status = InsufficientFunds;
            order.id = -1;

            emit orderStatusChanged(order);
        }
        else if (error == "Order already closed")
        {
            networkQueries.remove(reply);
        }
        else
        {
            qDebug() << "Error occured:" << error << reply->url() << r;
            if (!networkQueries.empty())
            {
                Order o = networkQueries.value(reply);
                qDebug() << o.toString();
            }
        }
        return;
    }

    if (reply->url().path() == api::getUserInfo)
    {
        Account uinfo;
//        std::fill_n(uinfo.total, int(UndefinedCurrency), -1);
//        std::fill_n(uinfo.available, int(UndefinedCurrency), -1);

        if (parseUserInfo(&result, uinfo))
        {
            Account userInfo = uinfo;
            emit balance(userInfo);
            return;
        }
        else
        {
            qCritical() << "Error while getting user info";
            return;
        }
    }
    else if (reply->url().path() == api::queryOrder)
    {
        Order order = networkQueries.take(reply);

        if (parseOrder(&result, order))
        {
            if (!inactiveOrders.contains(order.id))
            {
                QStringList pastOrders = settings.value("pastOrders").toStringList();
                QString id = QString::number(order.id);
                if (!pastOrders.contains(id))
                    pastOrders.push_front(id);
                settings.setValue("pastOrders", pastOrders);

                inactiveOrders.insert(order.id, order);
                emit orderStatusChanged(order);

            }
//            emit orderStatusChanged(order);
        }
        else
        {
            qCritical() << "Error while getting order";
            return;
        }
    }
    else if (reply->url().path() == api::placeOrder)
    {
        Order o = networkQueries.take(reply);

        if (!result.property("id").isNumber())
        {
            qCritical() << "placed orders id field is bad:"
                        << result.property("id").toString() << r;
            return;
        }
        qint64 id = result.property("id").toNumber();

        o.id = id;
        getOpenOrders();
        getBalance();
    }
    else if (reply->url().path() == api::closeOrder)
    {
        Order o = networkQueries.take(reply);

        if (!result.property("result").isString())
        {
            qCritical() << "closed orders field is bad:"
                        << result.property("result").toString() << r;
            return;
        }
        QString s = result.property("result").toString();

        if (s != "success")
        {
            qCritical() << "Error when closing orders" << r;
            return;
        }
        queryOrder(o);
        getBalance();
    }
    else if (reply->url().path() == api::getTicker)
    {
        Ticker ticker;
        if (parseTicker(&result, ticker))
        {
            analyzeTicker(ticker);
        }
        else
        {
            qCritical() << "Error while getting ticker";
            return;
        }
    }
    else if (reply->url().path() == api::getOrders)
    {
        int length = result.property("length").toInteger();
        QList<Order> orders;
        for(int i = 0; i < length; i++)
        {
            Order order;
            QScriptValue r = result.property(i);
            if (parseOrder(&r, order))
            {
                orders << order;
            }
            else
            {
                qCritical() << "Error while getting order";
                return;
            }
        }


        QHash<quint64, Order> maybeActiveOrders = activeOrders;
        activeOrders.clear();

        foreach (const Order &o, orders)
        {
            activeOrders.insert(o.id, o);
            if (!maybeActiveOrders.contains(o.id))
            {
                maybeActiveOrders.insert(o.id, o);
                emit orderStatusChanged(o);
            }
        }
        if (maybeActiveOrders.size() > activeOrders.size())
        {
            foreach (const Order &o, maybeActiveOrders)
            {
                if (!activeOrders.contains(o.id))
                    queryOrder(o);
            }
            getBalance();
        }
        if (activeOrders.size() > 0)
        {
            openOrderInterval *= 1.5;
            qDebug() << "Setting openOrderTimer to" << openOrderInterval;
            openOrderTimer.start(openOrderInterval);
        }
        return;
    }
    else
    {
        qDebug() << r << reply->url()
                 << reply->header(QNetworkRequest::ContentTypeHeader) << reply->errorString();
    }
}

void Kapiton::getOpenOrders()
{
#if QT_VERSION < 0x050000
    QUrl q;
#else
    QUrlQuery q;
#endif
    QString apiKey = settings.value("apiKey").toString();
    q.addQueryItem("api_key", apiKey);
    settings.setValue("apiKey", apiKey);
#if QT_VERSION < 0x050000
    QString postData = q.encodedQuery();
#else
    QString postData = q.query();
#endif
    postRequest(api::getOrders, postData);
}

void Kapiton::placeOrder(Order order)
{
#if QT_VERSION < 0x050000
    QUrl q;
#else
    QUrlQuery q;
#endif
    q.addQueryItem("amount", QString::number(order.amount.value, 'g', 16));
    q.addQueryItem("price", QString::number(order.price.value, 'g', 16));
    q.addQueryItem("type", order.type == Buy ? "bid" : order.type == Sell ? "ask" : "");
    QString apiKey = settings.value("apiKey").toString();
    q.addQueryItem("api_key", apiKey);
    settings.setValue("apiKey", apiKey);
#if QT_VERSION < 0x050000
    QString postData = q.encodedQuery();
#else
    QString postData = q.query();
#endif
    postRequest(api::placeOrder, postData, &order);
}

void Kapiton::closeOrder(Order order)
{
#if QT_VERSION < 0x050000
    QUrl q;
#else
    QUrlQuery q;
#endif
    q.addQueryItem("id", QString::number(order.id));
    QString apiKey = settings.value("apiKey").toString();
    q.addQueryItem("api_key", apiKey);
    settings.setValue("apiKey", apiKey);
#if QT_VERSION < 0x050000
    QString postData = q.encodedQuery();
#else
    QString postData = q.query();
#endif
    postRequest(api::closeOrder, postData, &order);
}

void Kapiton::queryOrder(Order order)
{
#if QT_VERSION < 0x050000
    QUrl q;
#else
    QUrlQuery q;
#endif
    q.addQueryItem("id", QString::number(order.id));
    QString apiKey = settings.value("apiKey").toString();
    q.addQueryItem("api_key", apiKey);
    settings.setValue("apiKey", apiKey);
#if QT_VERSION < 0x050000
    QString postData = q.encodedQuery();
#else
    QString postData = q.query();
#endif
    postRequest(api::queryOrder, postData, &order);
}

void Kapiton::getBalance()
{
#if QT_VERSION < 0x050000
    QUrl q;
#else
    QUrlQuery q;
#endif
    QString apiKey = settings.value("apiKey").toString();
    q.addQueryItem("api_key", apiKey);
    settings.setValue("apiKey", apiKey);
#if QT_VERSION < 0x050000
    QString postData = q.encodedQuery();
#else
    QString postData = q.query();
#endif
    postRequest(api::getUserInfo, postData);
}

void Kapiton::analyzeTicker(const Ticker &ticker)
{
    static Ticker lastTicker;

    if (lastTicker.price.price != ticker.price.price ||
            lastTicker.price.buy != ticker.price.buy ||
            lastTicker.price.sell != ticker.price.sell)
    {
        printTicker(ticker, m_pair);
        emit tick(ticker);
    }

    lastTicker = ticker;
}

void Kapiton::addOptions(std::vector<Broker*> *v, OptionParser *op)
{
    op->add(new Option::Multiple<Kapiton*, false, Broker*>(
                v, "kapiton", 'k', "Adds a Kapiton broker"));
}
const int Kapiton::classId = Factory<Broker>::registerClass(&(Kapiton::addOptions),
                                                       "kapiton");

