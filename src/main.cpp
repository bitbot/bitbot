#include <QCoreApplication>
#include <QTextStream>
#include <QSettings>

#include <QDir>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>

#include <QVariant>
#include <QDebug>

#if QT_VERSION < 0x050000
#include <QDesktopServices>
#else
#include <QStandardPaths>
#endif

#include "optionparser.h"
#include "factory.h"
#include "broker.h"
#include "trader.h"

#if QT_VERSION < 0x050000
void timestampedLog(QtMsgType type, const char *msg)
{
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "%s %s\n",
                qPrintable(QTime::currentTime().toString("HH:mm")), msg);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s\n", msg);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", msg);
        abort();
    }
}
#else
void timestampedLog(QtMsgType type, const QMessageLogContext &context,
                    const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "%s %s\n",
                qPrintable(QTime::currentTime().toString("HH:mm")),
                localMsg.constData());
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(),
                context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(),
                context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(),
                context.file, context.line, context.function);
        abort();
    }
    fflush(stderr);
}
#endif

namespace Option {

template <>
inline QString convertToType<QString>(std::string s, size_t)
{
    return QString(s.c_str());
}
}

bool openDb(QSqlDatabase &db)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
#if QT_VERSION < 0x050000
    QDir dir(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#else
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
#endif
    dir.mkpath(".");
    QString path(dir.filePath("bitbot.sqlite"));
    db.setDatabaseName(path);

    if (!db.open())
    {
        qCritical() << "Could not open database:" << db.lastError().text();
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
#if QT_VERSION < 0x050000
    qInstallMsgHandler(timestampedLog);
#else
    qInstallMessageHandler(timestampedLog);
#endif

    QCoreApplication::setOrganizationName("Mhn");
    QCoreApplication::setApplicationName("BitBot");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    QCoreApplication a(argc, argv);

    QSqlDatabase db;

    if (!openDb(db))
        return 1;

    OptionParser op(true);

    std::vector<Broker*> brokers;
    Factory<Broker>::addClassOptions(&brokers, &op);

    std::vector<Trader*> traders;
    Factory<Trader>::addClassOptions(&traders, &op);

    if (!op.parse(argc, argv, true))
        return 0;

    foreach (Trader* t, traders)
        if (!t->connectObjects(&brokers, &traders, &db))
            return 1;

    foreach (Broker* b, brokers)
        if (!b->connectObjects(&brokers, &traders, &db))
            return 1;

    foreach (Broker* b, brokers)
        if (!b->startServe())
            return 1;

    return a.exec();
}
