#include "trader.h"

Trader::Trader(QString name, size_t index, QObject *parent) :
    QObject(parent),
    m_name(name),
    m_index(index)
{
    qDebug("[%s] New Trader.", qPrintable(m_name));
    settings.beginGroup(m_name);
}

void Trader::rename(const QString name)
{
    settings.endGroup();
    m_name = name + "_" + m_name;
    settings.beginGroup(m_name);
    qDebug("[%s] reNew Trader.", qPrintable(m_name));
}
