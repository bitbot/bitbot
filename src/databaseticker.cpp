#include "databaseticker.h"

#include "factory.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>

#include <QVariant>

DatabaseTicker::DatabaseTicker(size_t index, QObject *parent) :
    Broker("DBT", index, parent),
    db(NULL),
    databaseName("ticker"),
    lastTickerTime(0)
{
    uint interval = settings.value("tickerinterval", 100).toUInt();
    interval = qMax(uint(1), interval);
    tickerTimer.setInterval(interval);
    settings.setValue("tickerinterval", interval);
    connect(&tickerTimer, SIGNAL(timeout()), this, SLOT(getTicker()));
}

bool DatabaseTicker::connectObjects(std::vector<Broker *> *, std::vector<Trader *> *, QSqlDatabase *db)
{
    this->db = db;

    if (!db->tables().contains(databaseName)) {
        qCritical() << "Table '" + databaseName + "' does not exist.";
        return false;
    }
    return true;
}

bool DatabaseTicker::startServe()
{
    tickerTimer.start();
    return true;
}

void printTicker(const Ticker &ticker)
{
    qDebug("[ %s ] price:%14.8f, buy:%14.8f, sell:%14.8f",
           qPrintable(ticker.date.toString()),
           ticker.price.price, ticker.price.buy, ticker.price.sell);
}

void DatabaseTicker::getTicker()
{
    QSqlQuery query;
    query.prepare("SELECT * FROM " + databaseName + " WHERE time > ? ORDER BY time LIMIT 1;");
    query.addBindValue(lastTickerTime);

    if (!query.exec()) {
        qCritical() << "Query error1: " << query.lastError().text();
    }
    while (query.next())
    {
        Ticker ticker;
        ticker.date = QDateTime::fromMSecsSinceEpoch(query.value(0).toULongLong());
        ticker.price.price = query.value(1).toReal();
        ticker.price.buy = query.value(2).toReal();
        ticker.price.sell = query.value(3).toReal();
        ticker.high = query.value(4).toReal();
        ticker.low = query.value(5).toReal();
        ticker.volume = query.value(6).toReal();
        ticker.monthAvg = query.value(7).toReal();
        ticker.monthVolume = query.value(8).toReal();
        ticker.remote = BTC;
        ticker.local = SEK;

        lastTickerTime = ticker.date.toMSecsSinceEpoch();

        printTicker(ticker);

        emit tick(ticker);
    }

}

bool DatabaseTicker::openDb()
{
    return true;
}

void DatabaseTicker::addOptions(std::vector<Broker*> *v, OptionParser *op)
{
    op->add(new Option::Multiple<DatabaseTicker*, false, Broker*>(
                v, "databaseticker", 'd', "Adds a ticker that reads from database (tickerdatabase)"));
}

const int DatabaseTicker::classId = Factory<Broker>::registerClass(
            &(DatabaseTicker::addOptions), "databaseticker");
