#ifndef STATISTICSWRITER_H
#define STATISTICSWRITER_H

#include "optionparser.h"
#include "trader.h"

class StatisticsWriter : public Trader
{
    Q_OBJECT
public:
    explicit StatisticsWriter(size_t index, QObject *parent = 0);

    bool connectObjects(std::vector<Broker*> *brokers, std::vector<Trader*> *, QSqlDatabase *);
    int getClassId() { return classId; }
    static void addOptions(std::vector<Trader*>* v, OptionParser* op);
public slots:
    void ticker(Ticker ticker);
    void balance(Account account);

private:
    void convertToCurrency(const Currency local, const Account &account, Account &localAccount) const;
    void writeStatistics2();

    bool gotTicker, gotAccount;
    Ticker m_tickers[UndefinedCurrency][UndefinedCurrency];
    Account m_account;
    static const int classId;
};

namespace Option {
template <>
inline StatisticsWriter* convertToType<StatisticsWriter*>(std::string, size_t index)
{
    return new StatisticsWriter(index);
}
}

#endif // STATISTICSWRITER_H
