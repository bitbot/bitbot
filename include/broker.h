#ifndef BROKER_H
#define BROKER_H

#include <QObject>

#include "types.h"

class Trader;
class QSqlDatabase;

class Broker : public QObject
{
    Q_OBJECT
public:
    explicit Broker(QString name, size_t index, QObject *parent = 0);

    virtual bool connectObjects(std::vector<Broker*> *,
                         std::vector<Trader*> *, QSqlDatabase *) { return true; }

    virtual bool startServe() = 0;
    virtual int getClassId() = 0;
    virtual bool claimWrite(int classId);
    virtual bool claimRead(int classId);
    const QString& name() { return m_name; }
    const size_t& index() { return m_index; }

signals:
    void tick(Ticker ticker);
    void orderStatusChanged(Order order);
    void balance(Account account);

public slots:
    virtual void placeOrder(Order order) = 0;
    virtual void closeOrder(Order order) = 0;
    
protected:
    void rename(const QString name);
    QString m_name;
    size_t m_index;
    QList<int> claimedReads, claimedWrites;
    QSettings settings;
};

#endif // BROKER_H
