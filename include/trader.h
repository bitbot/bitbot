#ifndef TRADER_H
#define TRADER_H

#include <QObject>

#include "broker.h"

class QSqlDatabase;

class Trader : public QObject
{
    Q_OBJECT
public:
    explicit Trader(QString name, size_t index, QObject *parent = 0);

    virtual bool connectObjects(std::vector<Broker*> *brokers,
                         std::vector<Trader*> *traders, QSqlDatabase *db) = 0;
    virtual int getClassId() = 0;
    const QString& name() { return m_name; }
    const size_t& index() { return m_index; }

signals:
    void placeOrder(Order order);
    void closeOrder(Order order);
//    void getBalance();
//    void getOpenOrders();
//    void queryOrder(Order order);

protected:
    void rename(const QString name);

    QString m_name;
    size_t m_index;
    QSettings settings;
};

#endif // TRADER_H
