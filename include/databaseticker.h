#ifndef DATABASETICKER_H
#define DATABASETICKER_H

#include "broker.h"
#include "optionparser.h"

#include <QTimer>

class DatabaseTicker : public Broker
{
    Q_OBJECT
public:
    explicit DatabaseTicker(size_t index, QObject *parent = 0);

    static void addOptions(std::vector<Broker *> *v, OptionParser *op);
    bool connectObjects(std::vector<Broker*> *, std::vector<Trader*> *, QSqlDatabase *db);
    bool startServe();
    int getClassId() { return classId; }

public slots:
    void placeOrder(Order) {}
    void closeOrder(Order) {}
    void queryOrder(Order) {}
    void getOpenOrders() {}
    void getBalance() {}
    void getTicker();

private:
    bool openDb();

    static const int classId;
    QSqlDatabase *db;
    QString databaseName;
    QTimer tickerTimer;
    quint64 lastTickerTime;

};

namespace Option {
template <>
inline DatabaseTicker* convertToType<DatabaseTicker*>(std::string, size_t index)
{
    return new DatabaseTicker(index);
}
}

#endif // DATABASETICKER_H
