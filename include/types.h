#ifndef TYPES_H
#define TYPES_H

#include <QDateTime>
#include <QSettings>
#include <QDebug>
#include <QStringList>

enum Currency { SEK, BTC, USD, PPC, EUR, LTC, NMC, RUR, NVC, TRC, FTC, XPM, CNH, GBP, DRK, DSH, ETH, UndefinedCurrency };

inline const char * currencyFormatString(Currency c)
{
    switch(c)
    {
    case RUR:
        return "%8.1f ";
    case SEK:
    case FTC:
    case TRC:
    case CNH:
        return "%8.2f ";
    case USD:
    case EUR:
    case NMC:
    case PPC:
    case XPM:
    case GBP:
        return "%8.3f ";
    case LTC:
    case NVC:
        return "%8.4f ";
    case BTC:
        return "%8.5f ";
    default:
        return "%8f ";
    }
}

inline const char * currencyToPlainString(Currency c)
{
    switch(c)
    {
    case SEK:
        return "SEK";
    case BTC:
        return "BTC";
    case USD:
        return "USD";
    case PPC:
        return "PPC";
    case EUR:
        return "EUR";
    case LTC:
        return "LTC";
    case NMC:
        return "NMC";
    case RUR:
        return "RUR";
    case NVC:
        return "NVC";
    case TRC:
        return "TRC";
    case FTC:
        return "FTC";
    case XPM:
        return "XPM";
    case CNH:
        return "CNH";
    case GBP:
        return "GBP";
    case DRK:
        return "DRK";
    case DSH:
        return "DSH";
    case ETH:
        return "ETH";
    default:
        return "UNK";
    }
}

inline Currency stringToCurrency(const char* s)
{
    if (strcmp(s, "SEK") == 0)
        return SEK;
    if (strcmp(s, "BTC") == 0)
        return BTC;
    if (strcmp(s, "USD") == 0)
        return USD;
    if (strcmp(s, "PPC") == 0)
        return PPC;
    if (strcmp(s, "EUR") == 0)
        return EUR;
    if (strcmp(s, "LTC") == 0)
        return LTC;
    if (strcmp(s, "NMC") == 0)
        return NMC;
    if (strcmp(s, "RUR") == 0)
        return RUR;
    if (strcmp(s, "NVC") == 0)
        return NVC;
    if (strcmp(s, "TRC") == 0)
        return TRC;
    if (strcmp(s, "FTC") == 0)
        return FTC;
    if (strcmp(s, "XPM") == 0)
        return XPM;
    if (strcmp(s, "CNH") == 0)
        return CNH;
    if (strcmp(s, "GBP") == 0)
        return GBP;
    if (strcmp(s, "DRK") == 0)
        return DRK;
    if (strcmp(s, "DSH") == 0)
        return DSH;
    if (strcmp(s, "ETH") == 0)
        return ETH;
    qDebug("UndefinedCurrency : %s", s);
    return UndefinedCurrency;
}


struct TradePair {
    TradePair() :
        local(UndefinedCurrency),
        remote(UndefinedCurrency),
        decimal_places(0),
        min_price(-1),
        max_price(-1),
        min_amount(-1),
        fee(-1),
        hidden(false)
    {}
    Currency local, remote;
    size_t decimal_places;
    qreal min_price, max_price, min_amount, fee;
    bool hidden;
};

struct Amount {
    Amount() :
        value(0),
        currency(UndefinedCurrency)
    {}
    qreal value;
    Currency currency;

    QString toString() const
    {
        QString s;
        return s.sprintf(currencyFormatString(currency), value) + currencyToPlainString(currency);
    }
};

struct Account {
    Account() {
        std::fill_n(total, int(UndefinedCurrency), -1);
        std::fill_n(available, int(UndefinedCurrency), -1);
        std::fill(&tradeTable[0][0], &tradeTable[0][0] + sizeof(tradeTable), (signed char)(-1));
    }
    qreal total[UndefinedCurrency], available[UndefinedCurrency];

    QList<TradePair> tradeInfo;
    signed char tradeTable[UndefinedCurrency][UndefinedCurrency];

    QString toString(qreal* cur)
    {
        QString s;
        for (int i = 0; i < UndefinedCurrency; i++)
        {
            if (cur[i] >= 0)
            {
                QString c;
                s += c.sprintf(currencyFormatString(Currency(i)), cur[i]) + currencyToPlainString(Currency(i)) + ",";
            }
        }
        if (s.length() > 0) s.remove(s.length() - 1);
        return s;
    }

    QString toString()
    {
        QString s;
        return s.sprintf("A%s",
//                         qPrintable(toString(total)),
                         qPrintable(toString(available))
                         );
    }
};

struct Price {
    Price() :
        price(-1),
        buy(-1),
        sell(-1)
    {}
    qreal price, buy, sell;
};

struct Ticker {
    Ticker() :
        local(UndefinedCurrency),
        remote(UndefinedCurrency),
        high(-1),
        low(-1),
        volume(-1),
        avg(-1),
        monthAvg(-1),
        monthVolume(-1)
    {}
    Price price;
    QDateTime date;
    Currency local, remote;
    qreal high, low, volume, avg, monthAvg, monthVolume;
};

enum Type { Buy, Sell, UndefinedType };

inline const char * typeToString(Type t)
{
    switch(t)
    {
    case Buy:
        return "BUY";
    case Sell:
        return "SELL";
    default:
        return "UNDF";
    }
}

inline Type stringToType(const char * s)
{
    if (strcmp(s, "BUY") == 0)
        return Buy;
    if (strcmp(s, "SELL") == 0)
        return Sell;
    return UndefinedType;
}

enum Status {
    Filled,
    Closed,
    Open,
    InsufficientFunds,
    Broken,
    PartiallyFilled,
    UndefinedStatus
};

inline const char * statusToString(Status t)
{
    switch(t)
    {
    case Filled:
        return "FLLD";
    case Closed:
        return "CLSD";
    case Open:
        return "OPEN";
    case InsufficientFunds:
        return "INSF";
    case Broken:
        return "BRKN";
    case PartiallyFilled:
        return "PRTF";
    default:
        return "UNDF";
    }
}

struct Order {
    Order() :
        type(UndefinedType),
        status(UndefinedStatus),
        id(0)
    {}
    Type type;
    Status status;
    QDateTime date;
    Amount amount, price;
    quint64 id;
    QString originatingTicker;
    QString toString() const
    {
        QString s;
        return s.sprintf("#%08lld %s %4s %s Price %s [%s]", id,
                         qPrintable(date.toString("yyyyMMdd hh:mm")),
                         typeToString(type), qPrintable(amount.toString()),
                         qPrintable(price.toString()),
                         statusToString(status)
                         );
    }
};

typedef QList<Order> OrderList;

#endif // TYPES_H
