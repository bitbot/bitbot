#ifndef BTCE_H
#define BTCE_H

#include <QObject>

#include <QTimer>
#include <QUrl>
#include <QHash>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QUrlQuery>

#include "broker.h"
#include "optionparser.h"

class QScriptValue;

class BtcE : public Broker
{
    enum Operation {
        opTrade,
        opGetInfo,
        opTransHistory,
        opActiveOrders,
        opCancelOrder,
        opPairInfo,
        opTicker,
        opTradeHistory
    };
    struct PostRequest {
        QNetworkRequest request;
        QUrlQuery data;
        Order order;
        Operation operation;
    };

    Q_OBJECT
public:
    explicit BtcE(size_t index, QObject *parent = 0);

    static void addOptions(std::vector<Broker *> *v, OptionParser *op);

    bool startServe();
    int getClassId() { return classId; }

    void sendNextPostRequest();
public slots:
    void placeOrder(Order order);
    void closeOrder(Order order);
    void getOpenOrders();
    void getBalance();

private slots:
    void getTicker();
    void replyFinished(QNetworkReply*reply);
    void getTransactions();

private:
    static const char *getMethod(Operation o);
    void postRequest(QUrlQuery postData, Operation o, Order order = Order());
    void getRequest(QString path, Operation o);

private:
    QList<PostRequest> networkQueue;
    QHash<QNetworkReply*, Order> networkQueries;
    QHash<QNetworkReply*, Operation> networkOperations;
    Account m_account;

    QHash<quint64, Order> inactiveOrders, activeOrders;
    QTimer tickerTimer, openOrderTimer;
    int openOrderInterval;
    bool sendAccount;


signals:

private slots:
    void analyzeTicker(const Ticker &ticker);

    static void printTicker(const Ticker &ticker, const TradePair &pair);

private:
    QNetworkAccessManager manager;

    static const int classId;
};

namespace Option {
template <>
inline BtcE* convertToType<BtcE*>(std::string, size_t index)
{
    return new BtcE(index);
}
}

#endif // BTCE_H
