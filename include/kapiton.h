#ifndef BITBOT_H
#define BITBOT_H

#include <QObject>

#include <QTimer>
#include <QUrl>
#include <QHash>
#include <QtNetwork/QNetworkAccessManager>

#include "broker.h"
#include "optionparser.h"

class QScriptValue;

class Kapiton : public Broker
{
    Q_OBJECT
public:
    explicit Kapiton(size_t index, QObject *parent = 0);

    static void addOptions(std::vector<Broker *> *v, OptionParser *op);

    bool startServe();
    int getClassId() { return classId; }

public slots:
    void placeOrder(Order order);
    void closeOrder(Order order);
    void queryOrder(Order order);
    void getOpenOrders();
    void getBalance();

private slots:
    void replyFinished(QNetworkReply*reply);

private:
    void postRequest(QString path, QString postData, Order *order = NULL);
    void getRequest(QString path, QString since = QString());

private:
    QHash<QNetworkReply*, Order> networkQueries;
    QHash<quint64, Order> inactiveOrders, activeOrders;
    QTimer tickerTimer, openOrderTimer;
    int openOrderInterval;





private:
    void updateOrderList(Order o);
    bool parseTicker(QScriptValue *result, Ticker &ticker);
    bool parseUserInfo(QScriptValue *result, Account &userInfo);
    bool parseOrder(QScriptValue *result, Order &order);
//    void getTrades(qint64 since);
//    void getOrderBook();
    void getUserInfo();
//    void withdrawBtc(qreal amount, QString address);

    void readSettings();
    void writeSettings();

signals:

private slots:
    void analyzeTicker(const Ticker &ticker);

    void getTicker();
    static void printTicker(const Ticker &ticker, const TradePair &pair);

private:
    QNetworkAccessManager manager;
    TradePair m_pair;

    static const int classId;
};

namespace Option {
template <>
inline Kapiton* convertToType<Kapiton*>(std::string, size_t index)
{
    return new Kapiton(index);
}
}

#endif // BITBOT_H
