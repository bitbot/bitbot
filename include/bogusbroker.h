#ifndef BOGUSBROKER_H
#define BOGUSBROKER_H

#include "broker.h"
#include "optionparser.h"

class BogusBroker : public Broker
{
    Q_OBJECT
public:
    explicit BogusBroker(size_t index, QObject *parent = 0);

    static void addOptions(std::vector<Broker *> *v, OptionParser *op);
    bool connectObjects(std::vector<Broker*> *brokers, std::vector<Trader*> *, QSqlDatabase *db);
    bool startServe();
    int getClassId() { return classId; }

public slots:
    void placeOrder(Order order);
    void closeOrder(Order order);
    void queryOrder(Order order);
    void getOpenOrders();
    void getBalance();

    void tickInput(Ticker ticker);

private:
    bool openDb();
    void getOrderDB(Order &order);
    qint64 insertOrderDB(Order &order);
    void setOrderStatusDB(Order &order, Status s);
    QList<qint64> getMaxMinOrderDB(Price &p);
    QList<Order> getOpenOrdersDB();
    void getAccountAvailable(Account &account);

    void fillOrder(Order o);
    Account readAccount();
    void writeAccount(Account account);

    static const int classId;
    QSqlDatabase *orderDB;
    QString databaseName;

};

namespace Option {
template <>
inline BogusBroker* convertToType<BogusBroker*>(std::string, size_t index)
{
    return new BogusBroker(index);
}
}

#endif // BOGUSBROKER_H
