#ifndef STUPIDCOWARD_H
#define STUPIDCOWARD_H

#include "optionparser.h"
#include "trader.h"

class SimpleTrader : public Trader
{
    Q_OBJECT

    static void addOptions(std::vector<Trader*>* v, OptionParser* op);
public:
    explicit SimpleTrader(size_t index, QObject *parent = 0);

    bool connectObjects(std::vector<Broker*> *brokers, std::vector<Trader*> *, QSqlDatabase *);
    int getClassId() { return classId; }
public slots:
    void analyzeTicker(Ticker ticker);
    void setBalance(Account balance);
    void getOrder(Order order);
    void closeOrders();
//    void openOrders(QList<Order>);

private:
    Account currentAccount;
    static const int classId;
    QHash<quint64, Order> m_openOrders;
    Order lastOrderOfType[UndefinedCurrency][UndefinedCurrency][2];
    Ticker lastTickers[UndefinedCurrency][UndefinedCurrency];
};

namespace Option {
template <>
inline SimpleTrader* convertToType<SimpleTrader*>(std::string, size_t index)
{
    return new SimpleTrader(index);
}
}

#endif // STUPIDCOWARD_H
