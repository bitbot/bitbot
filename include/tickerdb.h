#ifndef TICKERDB_H
#define TICKERDB_H

#include "optionparser.h"
#include "trader.h"

class TickerDB : public Trader
{
    Q_OBJECT
public:
    explicit TickerDB(size_t index, QObject *parent = 0);

    bool connectObjects(std::vector<Broker*> *brokers, std::vector<Trader*> *, QSqlDatabase *db);
    int getClassId() { return classId; }
    static void addOptions(std::vector<Trader*>* v, OptionParser* op);
public slots:
    void analyzeTicker(Ticker ticker);

private:
    bool openDb();

    QSqlDatabase *db;
//    QString m_broker;
    static const int classId;
};

namespace Option {
template <>
inline TickerDB* convertToType<TickerDB*>(std::string, size_t index)
{
    return new TickerDB(index);
}
}

#endif // TICKERDB_H
