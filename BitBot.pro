#-------------------------------------------------
#
# Project created by QtCreator 2013-04-15T10:49:49
#
#-------------------------------------------------

QT       += core network script sql
QMAKE_CXXFLAGS += -std=c++11

TARGET = BitBot
TEMPLATE = app

INCLUDEPATH += optionparser-cpp neuralnet-cpp include

SOURCES += src/main.cpp \
    src/trader.cpp \
    src/kapiton.cpp \
    src/broker.cpp \
    src/tickerdb.cpp \
    src/bogusbroker.cpp \
    src/databaseticker.cpp \
    src/statisticswriter.cpp \
    src/btc-e.cpp \
    src/simpletrader.cpp

HEADERS  += \
    include/trader.h \
    include/kapiton.h \
    include/broker.h \
    include/tickerdb.h \
    include/bogusbroker.h \
    include/types.h \
    include/databaseticker.h \
    include/statisticswriter.h \
    include/btc-e.h \
    include/simpletrader.h

